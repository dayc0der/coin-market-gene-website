"use strict";

window.addEventListener('DOMContentLoaded', function () {
  // START OF: is mobile =====
  function isMobile() {
    return (/Android|iPhone|iPad|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera);
  }
  // ===== END OF: is mobile

  const BODY = $('body');

  // START OF: mark is mobile =====
  (function() {
    if(isMobile()){
      BODY.addClass('body--mobile');
      $('video').remove()
    }else{
      BODY.addClass('body--desktop');
    }
  })();
  // ===== END OF: mark is mobile

  // START OF: loader =====
  var menu = (function(){
    let sidebar = $('.sidebar');
    let btn = $('._open_menu');

    function init(){
      btn.on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        BODY.toggleClass('menu-opened')
        $(this).toggleClass('active')
        sidebar.toggleClass('sidebar--show');
      })

      BODY.on('click', function (e) {
        sidebar.removeClass('sidebar--show')
        btn.removeClass('active')
      })

      sidebar.on('click', function (e) {
        e.stopPropagation();
      })

    }
    return {
      init: init
    }
  }());

  menu.init();

  $(function(){
    $("[data-toggle=popover]").popover({
      html : true,
      content: function() {
        var content = $(this).attr("data-popover-content");
        return $(content).children(".popover-body").html();
      },
      title: function() {
        var title = $(this).attr("data-popover-content");
        return $(title).children(".popover-heading").html()
      }
    });


    if ($('#price_popover').length) {
      $("[data-toggle=popover]").on('show.bs.popover', function () {
        $('.selectpicker_exchanges').selectpicker('refresh');
        console.log('c')


      })
    }
  });
});
